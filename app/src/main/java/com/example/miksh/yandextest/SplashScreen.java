package com.example.miksh.yandextest;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class SplashScreen extends AppCompatActivity {

    Activity activity;
    Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        activity = this;

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this).build();       //Getting realm config & instance
        realm = Realm.getInstance(realmConfiguration);

        letsGetItStarted();     //Launch Main function

    }

    private void letsGetItStarted() {

        DataLoader.getData(new DataLoader.DataDownloadListener<String>() {      //Launch function for loading data from the internet
            @Override
            public void success(JSONArray serverResponse) {                   //successful callback for gathering data
                DataLoader.jsonToRealm(activity, serverResponse);              //transfer received json to json parser and Realm objects maker

                SharedPreferences sharedPreferences = activity.getSharedPreferences(
                        "com.example.miksh.yandextest.INFO", Context.MODE_PRIVATE);
                sharedPreferences.edit().putBoolean("HasCache", true).apply();          //HasCash flag to SharedPref

                Intent intent1 = new Intent(SplashScreen.this, ArtistsList.class);          //Change application intent to ArtistsList
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent1);
                finish();

            }

            @Override
            public void error(String error) {           //gathering data error

                activity.runOnUiThread(new Runnable() {     //Creating buttons for load from cash and restart application
                    @Override
                    public void run() {

                        TextView errorMessage;
                        Button tryAgainBtn;
                        Button cacheBtn;
                        ProgressBar loadSpinner;

                        errorMessage = (TextView) findViewById(R.id.errorMessage);
                        tryAgainBtn = (Button) findViewById(R.id.tryAgainBtn);
                        cacheBtn = (Button) findViewById(R.id.cacheBtn);
                        loadSpinner = (ProgressBar) findViewById(R.id.loadSpinner);

                        errorMessage.setText(getResources().getString(R.string.noInternet));
                        errorMessage.setVisibility(View.VISIBLE);
                        tryAgainBtn.setVisibility(View.VISIBLE);
                        loadSpinner.setVisibility(View.GONE);

                        SharedPreferences sharedPreferences = activity.getSharedPreferences(
                                "com.example.miksh.yandextest.INFO", Context.MODE_PRIVATE);

                        if (sharedPreferences.getBoolean("HasCache", false)) {
                            cacheBtn.setVisibility(View.VISIBLE);

                            cacheBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent1 = new Intent(SplashScreen.this, ArtistsList.class);
                                    intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent1);
                                    finish();
                                }
                            });
                        }

                        tryAgainBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = getIntent();
                                finish();
                                startActivity(intent);
                            }
                        });

                    }
                });

            }
        });
    }
}
