package com.example.miksh.yandextest;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.miksh.yandextest.RealmObjects.Artist;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.view.SimpleDraweeView;

import io.realm.RealmBasedRecyclerViewAdapter;
import io.realm.RealmResults;
import io.realm.RealmViewHolder;

/**
 * Realm Recycler View Adapter
 */

public class ArtistsRRVAdapter extends RealmBasedRecyclerViewAdapter<Artist, ArtistsRRVAdapter.ViewHolder> {

    private Context currentActivity;

    public ArtistsRRVAdapter(
            Context context,
            RealmResults<Artist> realmResults,
            boolean automaticUpdate,
            boolean animateResults) {
        super(context, realmResults, automaticUpdate, animateResults);
        currentActivity = context;
    }

    public class ViewHolder extends RealmViewHolder {

        public TextView artistName;
        public TextView artistGenres;
        public TextView artistSongs;
        public SimpleDraweeView artistSmallPhoto;
        public CardView cardView;
        public long id;

        public ViewHolder(View container) {
            super(container);
            this.artistName = (TextView) container.findViewById(R.id.artistName);
            this.artistGenres = (TextView) container.findViewById(R.id.artistGenres);
            this.artistSongs = (TextView) container.findViewById(R.id.artistSongs);
            this.artistSmallPhoto = (SimpleDraweeView) container.findViewById(R.id.artistPhoto);
            this.cardView = (CardView) container.findViewById(R.id.artistCard);
        }
    }

    @Override
    public ViewHolder onCreateRealmViewHolder(ViewGroup viewGroup, int viewType) {
        View v = inflater.inflate(R.layout.artist_card, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindRealmViewHolder(final ViewHolder viewHolder, final int position) {
        final Artist artist = realmResults.get(position);
        final Uri uri = Uri.parse(artist.getCover().getSmall());

        viewHolder.artistName.setText(artist.getName());
        viewHolder.artistGenres.setText(Helper.parseGenres(artist.getGenres()));
        viewHolder.artistSongs.setText("Songs: ".concat(Integer.toString(artist.getTracks())).concat(", Albums: ").concat(Integer.toString(artist.getAlbums())));
        viewHolder.artistSmallPhoto.setImageURI(uri);
        viewHolder.id = artist.getId();

        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(currentActivity, ArtistInfo.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("id", viewHolder.id);
                currentActivity.startActivity(intent);
            }
        });
    }

}
