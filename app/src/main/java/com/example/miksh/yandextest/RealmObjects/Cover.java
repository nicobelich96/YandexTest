package com.example.miksh.yandextest.RealmObjects;

import io.realm.RealmObject;

/**
 * Cover class for creating Realm object.
 */
public class Cover extends RealmObject {
    private String small;
    private String big;

    public String getSmall() {
        return small;
    }

    public String getBig() {
        return big;
    }

    public void setSmall(String small) {
        this.small = small;
    }

    public void setBig(String big) {
        this.big = big;
    }
}
