package com.example.miksh.yandextest;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.miksh.yandextest.RealmObjects.Artist;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.view.SimpleDraweeView;
import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.nineoldandroids.view.ViewHelper;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * Artist's info
 */

public class ArtistInfo extends AppCompatActivity implements ObservableScrollViewCallbacks {

    public SimpleDraweeView artistBigPhoto;
    private View mToolbarView;
    private ObservableScrollView mScrollView;
    private int mParallaxImageHeight;
    private Realm realm;
    private TextView description;
    private TextView genres;
    private TextView creations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_info);

        buildArtistInfo();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (realm != null) {
            realm.close();
            realm = null;
        }
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {

        int baseColor = ContextCompat.getColor(this, R.color.colorPrimaryDark);
        float alpha = Math.min(1, (float) scrollY / mParallaxImageHeight);
        mToolbarView.setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, baseColor));
        ViewHelper.setTranslationY(artistBigPhoto, scrollY / 2);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        onScrollChanged(mScrollView.getCurrentScrollY(), false, false);
    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
        ActionBar ab = getSupportActionBar();
        if (scrollState == ScrollState.UP) {
            if (ab.isShowing()) {
                ab.hide();
            }
        } else if (scrollState == ScrollState.DOWN) {
            if (!ab.isShowing()) {
                ab.show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void buildArtistInfo() {

        Intent intent = getIntent();                                //Get Artist's ID from extras
        long position = intent.getLongExtra("id", 0);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));  //ToolBar settings
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mToolbarView = findViewById(R.id.toolbar);
        mToolbarView.setBackgroundColor(
                ScrollUtils.getColorWithAlpha(0, ContextCompat.getColor(this, R.color.colorPrimary)));

        mScrollView = (ObservableScrollView) findViewById(R.id.scrollView);     //Observable ScrollView settings
        assert mScrollView != null;
        mScrollView.setScrollViewCallbacks(this);

        mParallaxImageHeight = getResources().getDimensionPixelSize(
                R.dimen.parallax_image_height);

        RealmConfiguration realmConfiguration = new RealmConfiguration      //Getting Realm
                .Builder(this)
                .build();
        realm = Realm.getInstance(realmConfiguration);

        RealmResults<Artist> result = realm.where(Artist.class)             //Searching artist in Realm
                .equalTo("id", position)
                .findAll();

        artistBigPhoto = (SimpleDraweeView) findViewById(R.id.imageParallax);   //Filling artist's data
        final Uri uri = Uri.parse(result.get(0).getCover().getBig());

        description = (TextView) findViewById(R.id.description);
        genres = (TextView) findViewById(R.id.genres);
        creations = (TextView) findViewById(R.id.creations);

        artistBigPhoto.setImageURI(uri);
        description.setText(result.get(0).getName().concat(" ").concat(result.get(0).getDescription()));
        genres.setText(Helper.parseGenres(result.get(0).getGenres()));
        creations.setText("Songs: ".concat(Integer.toString(result.get(0).getTracks())).concat("  •  Albums: ").concat(Integer.toString(result.get(0).getAlbums())));

    }

}
