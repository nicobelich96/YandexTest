package com.example.miksh.yandextest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import com.example.miksh.yandextest.RealmObjects.Artist;
import com.facebook.drawee.backends.pipeline.Fresco;

import co.moonmonkeylabs.realmrecyclerview.RealmRecyclerView;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * List of Artists
 */

public class ArtistsList extends AppCompatActivity {

    private RealmRecyclerView realmRecyclerView;
    private ArtistsRRVAdapter artistsRRVAdapter;
    private Realm realm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artists_list);

        RealmConfiguration realmConfiguration = new RealmConfiguration
                .Builder(this)
                .build();

        realm = Realm.getInstance(realmConfiguration);          //Getting Realm Instance

        realmRecyclerView = (RealmRecyclerView) findViewById(R.id.artistsRRV);
        Fresco.initialize(this);                                 //Fresco initialization

        RealmResults<Artist> result = realm.where(Artist.class)     //Realm searching
                .findAll();

        artistsRRVAdapter = new ArtistsRRVAdapter(          //Making adapter for Realm Recycler View
                this,
                result,
                true,
                true);

        realmRecyclerView.setAdapter(artistsRRVAdapter);    //Setting adapter

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (realm != null) {
            realm.close();
            realm = null;
        }
    }
}