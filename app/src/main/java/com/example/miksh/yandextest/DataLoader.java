package com.example.miksh.yandextest;

import android.app.Activity;
import android.util.Log;

import com.example.miksh.yandextest.RealmObjects.Artist;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class DataLoader {

    public interface DataDownloadListener<String> {     //Callback interface for Data loading
        void success(JSONArray serverResponse);

        void error(String error);
    }

    public static void getData(final DataDownloadListener<String> myCallback) {     //Data loader

        HttpUrl.Builder urlBuilder = HttpUrl.parse("http://download.cdn.yandex.net/mobilization-2016/artists.json").newBuilder();
        String url = urlBuilder.build().toString();

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .build();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                myCallback.error(e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                }

                final String responseData = response.body().string();

                try {
                    JSONArray artistsJsonArr = new JSONArray(responseData);
                    myCallback.success(artistsJsonArr);

                } catch (Exception e) {
                    myCallback.error(e.toString());
                }

            }
        });

    }

    public static void jsonToRealm(Activity activity, JSONArray json) {     //Json parser

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(activity).build();
        Realm realm = Realm.getInstance(realmConfiguration);

        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();

        try {
            for (int index = 0; index < json.length(); index++) {
                realm.beginTransaction();                                               //Parse Json to Realm objects
                realm.createObjectFromJson(Artist.class, json.getJSONObject(index));
                realm.commitTransaction();
            }


        } catch (Exception e) {

        }
    }

}
