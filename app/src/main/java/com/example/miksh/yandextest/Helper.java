package com.example.miksh.yandextest;

/**
 * Class for helper functions
 */
public class Helper {

    public static String parseGenres(String input) {        //Parser of genres string
                                                            //Because of Realm still in beta and does not support simple types arrays yet
        if (input.length() > 4) {

            return input.substring(2, input.length() - 2).replaceAll("\",\"", ", ");
        }

        return "";
    }

}
